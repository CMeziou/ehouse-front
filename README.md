### Projet eHouse-front - Frontend Next.js pour l'API de Maison Connectée


## Description
Le projet eHouse-front est le frontend Next.js pour l'API de maison connectée. Il utilise Next.js pour consommer l'API Symfony, Axios pour les requêtes HTTP, les composants de Next.js pour une architecture modulaire, et les fonctionnalités telles que useState, useEffect, SSR (Server-Side Rendering)...

## Fonctionnalités principales
- **Consommation de l'API :** Intégration de l'API Symfony pour récupérer les données de la maison connectée.
- **Affichage dynamique :** Utilisation de composants de Next.js pour organiser le code de manière modulaire.
- **Gestion d'état :** Utilisation de useState pour gérer l'état local des composants.
- **Effets secondaires :** Utilisation de useEffect pour gérer les effets secondaires tels que les requêtes asynchrones.
- **Server-Side Rendering (SSR) :** Amélioration des performances en rendant certaines pages côté serveur.

## Technologies utilisées
- Next.js
- React
- Typescript
- Axios
- Server-Side Rendering (SSR)

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/eHouse-front.git`
2. Installez les dépendances avec npm `npm install`
3. Lancez l'application avec `npm run dev`

## Configuration
Modifiez le fichier `config.js` pour définir l'URL de l'API Symfony.

## Auteurs
- Chems MEZIOU
