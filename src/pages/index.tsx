import { House } from "@/entities";
import { HouseService } from "@/house-service";
import { GetServerSideProps } from "next";
import Link from "next/link";

interface Props {
  houses: House[];
}
export default function About({ houses }: Props) {
  return (
    <div className="container mt-5">
      <div className="rounded-xl shadow p-3" style={{backgroundColor: "#ffffffcd"}}>
        <h1
          className="display-5 text-center mb-4"
          style={{
            background: "-webkit-linear-gradient(blue, violet)",
            WebkitBackgroundClip: "text",
            WebkitTextFillColor: "transparent",
            fontWeight: "bolder"
          }}
        >
          WebWave Home: <br/>Riding the Smart Tech Wave
        </h1>
        {/* <h1 className="display-4 text-center mb-4">
          <span className="bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500">
            <span className="text-blue-600">WebWave Home:</span> Riding the Smart Tech Wave
          </span>
        </h1> */}
        <img
          src={"https://source.unsplash.com/random/1000x500?smart-home"}
          alt="img-acceuil"
          className="img-thumbnail mx-auto d-block mb-5 mt-5 shadow"
          style={{ width: "60%" }}
        />
        <div className="lead p-5">
          <p>
            A potato cannon is a pipe-based cannon that uses air pressure
            (pneumatic), or combustion of a flammable gas (aerosol, propane,
            etc.), to launch projectiles at high speeds. They are built to fire
            chunks of potato, as a hobby, or to fire other sorts of projectiles,
            for practical use. Projectiles or failing guns can be dangerous and
            result in life-threatening injuries, including cranial fractures,
            enucleation, and blindness if a person is hit.
          </p>
        </div>
        <div className="row m-5 d-flex align-items-center justify-content-center g-5">
          {houses &&
            houses.map((house) => (
              <div key={house.id} className="col-12 col-md-4">
                <div className="card text-center shadow rounded">
                  <img
                    src={
                      "https://source.unsplash.com/random/200x200?" + house.name
                    }
                    className="img-thumbnail"
                    alt={house.name}
                  />
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
  
}

export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const houses = await HouseService.fetchAllHouse();

  try {
    return {
      props: {
        houses,
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};
