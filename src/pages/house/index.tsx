import React, { useState } from "react";
import { GetServerSideProps } from "next";
import { HouseService } from "@/house-service";
import AllHousesComponent from "@/components/AllHousesComponent";
import Modal from "react-modal";
import { House, Room } from "@/entities";
import { RoomService } from "@/room-service";

Modal.setAppElement("#__next");

interface Props {
  houses: House[];
  rooms: Room[];
}

export default function index({ houses: initialHouses, rooms }: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [newHouseName, setNewHouseName] = useState("");
  const [houses, setHouses] = useState(initialHouses);

  const handleDeleteHouse = async (id: number) => {
    try {
      await HouseService.deleteHouse(id);
      setHouses((prevHouses) => prevHouses.filter((house) => house.id !== id));
    } catch (error) {
      console.error("Error deleting house:", error);
    }
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setNewHouseName("");
  };

  const handleAddHouse = async () => {
    try {
      const newHouse = await HouseService.createHouse({ name: newHouseName });
      setHouses((prevHouses: any) => [...prevHouses, newHouse]); // Mettre à jour l'état des maisons
      closeModal();
    } catch (error) {
      console.error("Error adding house:", error);
    }
  };

  return (
    <>
      <button
        className="btn btn-success px-4 py-1"
        style={{ marginLeft: "4em", marginTop: "4em", background: "purple" }} 
        onClick={openModal}
      >
        Add house
      </button>
      <AllHousesComponent
        houses={houses}
        rooms={rooms}
        onDeleteHouse={handleDeleteHouse}
      />
      <Modal isOpen={isModalOpen} onRequestClose={closeModal} className="text-center mt-5 py-4 px-4">
        <h2>Add a New House</h2>
        <label htmlFor="exampleInputEmail1" className="form-label">
            Device Name
          </label>
        <div className="mb-3">
        <input
          type="text"
          placeholder="House Name"
          value={newHouseName}
          onChange={(e) => setNewHouseName(e.target.value)}
        />
        </div>
        <button onClick={handleAddHouse} className=" btn btn-success m-1 rounded text-white" style={{width: "5em"}}>Add</button>
        <button onClick={closeModal} className=" btn btn-warning m-1 rounded" style={{width: "5em"}}>Cancel</button>
      </Modal>  
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const houses = await HouseService.fetchAllHouse();
  const rooms = await RoomService.fetchAllRooms();

  return {
    props: {
      houses,
      rooms,
    },
  };
};
