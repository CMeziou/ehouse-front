import HouseComponent from "@/components/HouseComponent";
import React, { useState } from "react";
import { House, Room } from "@/entities";
import { HouseService } from "@/house-service";
import { RoomService } from "@/room-service";
import { GetServerSideProps } from "next";
import Modal from "react-modal";

Modal.setAppElement("#__next");

interface Props {
  house: House;
  rooms: Room[];
}

export default function house({
  house: initialHouses,
  rooms: initialRooms,
}: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [newRoomName, setNewRoomName] = useState("");
  const [houses, setHouses] = useState(initialHouses);
  const [room, setRoom] = useState(initialRooms);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setNewRoomName("");
  };

  const handleAddRoom = async () => {
    try {
      const newRoom = await RoomService.createRoomIntoHouse({
        name: newRoomName,
        createdAt: new Date(),
        house: houses,
      });
      setRoom((prevRooms: any) => [...prevRooms, newRoom]);
      closeModal();
    } catch (error) {
      console.error("Error adding room:", error);
    }
  };
  const handleDeleteRoom = async (id: number) => {
    try {
      await RoomService.delteteRoom(id);
      setRoom((prevRoom) => prevRoom.filter((room) => room.id !== id));
    } catch (error) {
      console.error("Error deleting room:", error);
    }
  };

  return (
    <>
      <button
        className="btn btn-success px-4 py-1"
        style={{ marginLeft: "4em", marginTop: "4em", background: "purple" }}
        onClick={openModal}
      >
        Add room
      </button>
      <HouseComponent
        house={houses}
        rooms={room}
        onDeleteRoom={handleDeleteRoom}
      />
      ;
      <Modal isOpen={isModalOpen} onRequestClose={closeModal} className="text-center mt-5 py-4 px-4">
        <h2>Add a New Room</h2>
        <label htmlFor="exampleInputEmail1" className="form-label">
          Room Name
        </label>
        <div className="mb-3">
        <input
          type="text"
          placeholder="Room Name"
          value={newRoomName}
          onChange={(e) => setNewRoomName(e.target.value)}
        />
        </div>
        <button onClick={handleAddRoom} className=" btn btn-success m-1 rounded text-white" style={{width: "5em"}}>Add</button>
        <button onClick={closeModal} className=" btn btn-warning m-1 rounded" style={{width: "5em"}}>Cancel</button>
      </Modal>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const { id } = context.query;
  const house = await HouseService.fetchOneHouse(Number(id));
  const rooms = await RoomService.fetchRoomsByHouse(id);

  try {
    return {
      props: {
        house,
        rooms,
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};
