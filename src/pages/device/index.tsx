import React, { useState } from 'react';
import { GetServerSideProps } from 'next';
import { DeviceService } from '@/device-service';
import { Device } from '@/entities';
import DeviceComponent from '@/components/DeviceComponent';

interface Props {
  devices: Device[];
}

export default function device({ devices: initialDevice }: Props) {
  const [device, setDevice] = useState(initialDevice); 

  const handleDeleteDevice = async (id: number) => {
    try {
      await DeviceService.delteteDevice(id);
      setDevice(prevDevice => prevDevice.filter(setDevice => setDevice.id !== id));
    } catch (error) {
      console.error("Error deleting house:", error);
    }
  };
  return (
    <>
      <DeviceComponent devices={device} onDeleteDevice={handleDeleteDevice} />
    </>
  );
  };


export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const devices = await DeviceService.fetchAllDevices();
  return {
    props: {
      devices,
    },
  };
};
