import '@/styles/globals.css';
import type { AppProps } from 'next/app';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Navbar from '@/components/Navbar';
import { useEffect } from 'react';

export default function App({ Component, pageProps }: AppProps) {

  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);

  return(
    <>
      <div style={{background: "linear-gradient(to right, #4b6cb7, #182848)", minHeight: "100vh"}}>
        <Navbar />
        <Component {...pageProps} />
      </div>
    </>
  ) ;
}
