import RoomComponent from "@/components/RoomComponent";
import { Device, Room } from "@/entities";
import { RoomService } from "@/room-service";
import { GetServerSideProps } from "next";
import { useState } from "react";
import { DeviceService } from "@/device-service";
import Modal from "react-modal";

Modal.setAppElement("#__next");

interface Props {
  rooms: Room;
  devices: Device[];
}

export default function RoomPage({
  rooms: initialRooms,
  devices: initialDevice,
}: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [newDeviceName, setNewDeviceName] = useState("");
  const [room, setRoom] = useState(initialRooms);
  const [device, setDevice] = useState(initialDevice);
  const [type, setType] = useState("");
  const [status, setStatus] = useState("");

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setNewDeviceName("");
  };

  const handleDeleteDevice = async (id: number) => {
    try {
      await DeviceService.delteteDevice(id);
      const updatedDevices = initialDevice.filter((device) => device.id !== id);
      setDevice(updatedDevices);
    } catch (error) {
      console.error("Error deleting device:", error);
    }
  };

  const handleAddDevice = async () => {
    try {
      const newDevice = await RoomService.createDevice({
        name: newDeviceName,
        type: type,
        status: status,
        createdAt: new Date(),
        room: room,
      });
      setDevice((prevRooms: any) => [...prevRooms, newDevice]);
      
      setNewDeviceName("");
      setType("");
      setStatus("");
      
      closeModal();
    } catch (error) {
      console.error("Error adding house:", error);
    }
  };
  

  return (
    <>
      <button
        className="btn -btn-success px-4 py-1 text-white"
        style={{ marginLeft: "4em", marginTop: "4em", background: "purple" }}
        onClick={openModal}
      >
        Add devices
      </button>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        className="text-center mt-5 py-4 px-4"
      >
        <h2>Add a new device</h2>
        <label htmlFor="exampleInputEmail1" className="form-label">
          Device Name
        </label>
        <div className="mb-3">
          <input
            type="text"
            placeholder="Device Name"
            value={newDeviceName}
            onChange={(e) => setNewDeviceName(e.target.value)}
          />
        </div>
        <label htmlFor="exampleInputPassword1" className="form-label">
          Device Type
        </label>
        <div className="mb-3">
          <input
            type="text"
            placeholder="Device Type"
            value={type}
            onChange={(e) => setType(e.target.value)}
          />
        </div>
        <label htmlFor="exampleInputPassword1" className="form-label">
          Device Status
        </label>
        <div className="mb-3">
          <input
            type="text"
            placeholder="Device Status"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
          />
        </div>
        <button
          onClick={handleAddDevice}
          className=" btn btn-success m-1 rounded text-white"
          style={{ width: "5em" }}
        >
          Add
        </button>
        <button
          onClick={closeModal}
          className=" btn btn-warning m-1 rounded"
          style={{ width: "5em" }}
        >
          Cancel
        </button>
      </Modal>
      <RoomComponent
        room={room}
        initialDevices={device}
        onDeleteDevice={handleDeleteDevice}
      />
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const { id } = context.query;
  try {
    const room = await RoomService.fetchSingleRoom(id);
    const device = await DeviceService.fetchDeviceByRoom(Number(id));
    return {
      props: {
        rooms: room,
        devices: device,
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};
