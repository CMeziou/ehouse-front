import RoomsComponent from "@/components/AllRoomsComponent";
import { DeviceService } from "@/device-service";
import { Device, House, Room } from "@/entities";
import { HouseService } from "@/house-service";
import { RoomService } from "@/room-service";
import { GetServerSideProps } from "next";
import React, { useState } from "react";

interface Props {
  rooms: Room[];
  houses: House[];
  devices?: Device[];
}

function room({ rooms: initialRooms, devices, houses }: Props) {
  const [room, setRoom] = useState(initialRooms); 

  const handleDeleteRoom = async (id: number) => {
    try {
      await RoomService.delteteRoom(id);
      setRoom(prevRoom => prevRoom.filter(room => room.id !== id));
    } catch (error) {
      console.error("Error deleting house:", error);
    }
  };
  return (
    <>
      <h1 className="text-center text-white pt-5 mt-3">All Rooms</h1>
      <RoomsComponent rooms={room} onDeleteRoom={handleDeleteRoom} houses={houses} />
    </>
  );
}

export default room;

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const rooms = await RoomService.fetchAllRooms();
  const devices = await DeviceService.fetchAllDevices();
  const houses = await HouseService.fetchAllHouse();
  return {
    props: {
      rooms,
      devices,
      houses
    }
  };
};
