export interface House{
    id?:number;
    name:string;
}

export interface Room{
    id?:number;
    name?:string;
    createdAt:Date;
    house:House;
}

export interface Device{
    id?:number;
    name:string;
    type:string;
    status:string;
    createdAt:Date;
    room:Room;
}
