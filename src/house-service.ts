import axios from "axios";
import { House, Room } from "./entities";

export const HouseService = {
    async fetchAllHouse(){
        const response = await axios.get<House[]>('http://localhost:8000/api/house');
        return response.data
    },
    async fetchOneHouse(id: number){
        const response = await axios.get<House>('http://localhost:8000/api/house/' + id);
        return response.data
    },
    async createHouse(house:House){
        const response = await axios.post<House[]>('http://localhost:8000/api/house', house);
        return response.data
    },
    async updateHouse(house:House){
        const response = await axios.put<House>('http://localhost:8000/api/house/' + house.id, house);
        return response.data
    },
    async deleteHouse(id: number){
        const response = await axios.delete<House>('http://localhost:8000/api/house/' + id);
        return response.data
      }
}