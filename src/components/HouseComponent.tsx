import React from "react";
import { House, Room } from "../entities";
import RoomsComponent from "./AllRoomsComponent";

interface Props {
  house: House;
  rooms: Room[];
  onDeleteRoom: (id: number) => void;
}
function HouseComponent({ house, rooms, onDeleteRoom }: Props) {
  
  return (
    <>
      <div className="card text-center m-5" style={{background: "linear-gradient(65deg, #3498db, #966ea2)"}}>
          <h1 className="text-white" key={house.id}>
            <b>{house.name}</b>
          </h1>
        <div className="card-header">
          <h5 className="card-title text-info" style={{ height: "2rem" }}>
            Bienvenue dans votre maison connecter
          </h5>
          <img
            src={
              "https://source.unsplash.com/random/1000x500?/" + house.name
            }
            style={{width: "400em", height: "40em"}}
            className="card-img img-thumbnail"
            alt={house.name}
            />
        </div>
        <RoomsComponent rooms={rooms} onDeleteRoom={onDeleteRoom} houses={[house]} />
      </div>
    </>
  );
}

export default HouseComponent;
