import { House, Room } from "@/entities";
import Link from "next/link";

interface Props {
  houses: House[];
  rooms: Room[];
  onDeleteHouse: (id: number) => void;
}

export default function AllHousesComponent({
  houses,
  rooms,
  onDeleteHouse,
}: Props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-3 col-lg-3 text-center">
          <ul className="list-group mt-3">
            {houses.map((house) => (
                <Link
                  href={`/house/${house.id}`}
                  className="text-decoration-none"
                  key={house.id}
                >
              <li className="list-group-item m-1 hover" key={house.id}>
                  {house.name}
              </li>
                </Link>
            ))}
          </ul>
        </div>
        <div className="col-md-9 col-lg-9 g-4">
          <div className="row">
            {houses.map((house) => (
              <div key={house.id} className="col-lg-4 col-md-6 col-sm-12 mb-4">
                <div className="card hover ">
                <Link href={`/house/${house.id}`} className="text-decoration-none">
                  <h5
                    className="card-header text-center"
                    style={{ background: "powderblue" }}
                  >
                    {house.name}
                  </h5>
                  </Link>
                  <div
                    className="card-body text-center"
                    style={{ background: "lightblue" }}
                  >
                    <h5 className="card-title">Lorem ipsum dolor sit amet</h5>
                    <p className="card-text">
                      They are {""}
                      {
                        rooms.filter((ro) => ro.house?.id === house.id).length
                      }{" "}
                      rooms {""} in this house
                    </p>
                    <button
                      className="btn btn-danger"
                      onClick={() => onDeleteHouse(Number(house.id))}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
