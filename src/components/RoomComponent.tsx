import { Device, Room } from "@/entities";
import DeviceComponent from "./DeviceComponent";

interface Props {
  room: Room;
  initialDevices: Device[];
  onDeleteDevice: (id: number) => void;
}

function RoomComponent({ room, initialDevices, onDeleteDevice }: Props) {
  return (
    <>
      <div className="card text-center m-5" style={{ background: "linear-gradient(65deg, #3498db, #966ea2)" }}>
        <h1 className="text-white" key={room.id}>
          <b>{room.name}</b>
        </h1>
        <div className="card-header">
          <img
            src={'https://source.unsplash.com/random/1000x500?/' + room.name}
            style={{ width: "250em", height: "40em" }}
            className="img-fluid"
            alt="card-img"
          />
        </div>
        <DeviceComponent devices={initialDevices} onDeleteDevice={onDeleteDevice} />
      </div>
    </>
  );
}

export default RoomComponent;
