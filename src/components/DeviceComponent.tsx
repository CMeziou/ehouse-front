import React, { useEffect, useState } from "react";
import { Device, House } from "@/entities";
import { PlayIcon } from "@heroicons/react/20/solid";

interface Props {
  devices: Device[];
  onDeleteDevice: (id: number) => void;
}

function DeviceComponent({ devices, onDeleteDevice }: Props) {
  const [updatedDevices, setUpdatedDevices] = useState(devices);

  useEffect(() => {
    setUpdatedDevices(devices);
  }, [devices]);
  const handleUpdateDevice = (deviceToUpdate: Device) => {
    const updatedDevice = { ...deviceToUpdate };
    updatedDevice.status = updatedDevice.status === "on" ? "off" : "on";

    const updatedDeviceList = updatedDevices.map((device) =>
      device.id === updatedDevice.id ? updatedDevice : device
    );

    setUpdatedDevices(updatedDeviceList);

  };

  function playText(device: Device) {
    const text = `Currently, ${device.name} is in a state of ${device.status}`;
    const speech = new SpeechSynthesisUtterance(text);
    speech.lang = "en-EN";
    speech.rate = 0.9;
    speech.volume = 0.5;
    speechSynthesis.speak(speech);
  }

  return (
    <>
      <div className="card-body d-flex flex-row">
        <div className="row m-5 d-flex items-center justify-content-center g-4">
          {updatedDevices &&
            updatedDevices.map((device) => (
              <div
                key={device.id}
                className="col-12 col-sm-6 col-md-4 col-lg-3"
              >
                <div
                  className="card text-center"
                  style={{
                    boxShadow: "2px 4px 6px rgba(0.5, 0.5, 0.5, 0.2)",
                  }}
                >
                  <div className="row no-gutters">
                    <img
                      src={
                        "https://source.unsplash.com/random/1000x500?/" +
                        device.name
                      }
                      className="card-img-left"
                      alt="..."
                    />
                    <div className="card-body">
                      <h5 className="card-title" style={{ height: "3rem" }}>
                        {device.room.name} appliance
                      </h5>
                      <p className="card-text">Name: {device.name}</p>
                      <p className="card-text">Type: {device.type}</p>
                      <div className="form-check form-switch d-flex justify-content-center">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          role="switch"
                          id={`flexSwitchCheckChecked-${device.id}`}
                          onChange={() => handleUpdateDevice(device)}
                          checked={device.status === "on"}
                        />
                        <p className="card-text mx-2">Status: {device.status}</p>
                      </div>
                      <p className="card-text">
                        Created at:{" "}
                        {new Date(device.createdAt).toLocaleDateString(
                          "en-EN"
                        )}
                      </p>
                      <button
                        className="m-2 btn btn-danger border-rouded"
                        onClick={() => onDeleteDevice(Number(device.id))}
                      >
                        Delete
                      </button>
                      <button
                        className="btn btn-info m-2"
                        style={{ opacity: "0.8" }}
                        onClick={() => playText(device)}
                      >
                        <PlayIcon
                          style={{
                            width: "0.7rem",
                            height: "0.7rem",
                            color: "white",
                          }}
                        />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </>
  );
}

export default DeviceComponent;
