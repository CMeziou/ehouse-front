// import { useState } from "react";
// import { Device } from "@/entities";
// import { DeviceService } from "@/device-service";

// interface Props {
//   devices: Device[];
// }

// export default function DevicesOnComponent({ devices }: Props) {
//   const [isTurningOff, setIsTurningOff] = useState(false);
//   const [updatedDevices, setUpdatedDevices] = useState(devices); // State to hold updated devices

//   const handleTurnOffAllDevices = async () => {
//     if (!isTurningOff) {
//       try {
//         setIsTurningOff(true);
//         const devicesToTurnOff = updatedDevices.filter(
//           (device) => device.status === "on"
//         );

//         for (const device of devicesToTurnOff) {
//           await DeviceService.updateDevice({ ...device, status: "off" });
//         }

//         const updatedDevicesList = await DeviceService.fetchAllDevices();
//         setUpdatedDevices(updatedDevicesList);
//       } catch (error) {
//         console.error("Error turning off devices:", error);
//       } finally {
//         setIsTurningOff(true);
//       }
//     }
//   };

//   return (
//     <div>
//       <div className="card-body d-flex flex-row">
//         <div className="row m-5 d-flex items-center justify-center g-4">
//           {/* <input
//             className="form-check-input"
//             type="checkbox"
//             role="switch"
//             id="flexSwitchCheckChecked"
//             onChange={handleTurnOffAllDevices}
//           /> */}
//           {updatedDevices
//             .filter((fil) => fil.status === "on")
//             .map((device) => (
//               <div
//                 key={device.id}
//                 className="col-12 col-sm-6 col-md-4 col-lg-3"
//               >
//                 <div className="card text-center">
//                   <div className="row no-gutters">
//                     <img
//                       src={
//                         "https://source.unsplash.com/random/1000x500?" +
//                         device.name
//                       }
//                       className="card-img-left"
//                       alt="..."
//                     />
//                     <div className="card-body">
//                       <h5 className="card-title" style={{ height: "3rem" }}>
//                         {device.room.name}
//                       </h5>
//                       <p className="card-text">Name: {device.name}</p>
//                       <p className="card-text">Type: {device.type}</p>
//                       <p className="card-text">Status: {device.status}</p>
//                       <p className="card-text">
//                         Created at:{" "}
//                         {new Date(device.createdAt).toLocaleDateString("en-EN")}
//                       </p>
//                       <a href="#" className="btn btn-primary">
//                         See devices!
//                       </a>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             ))}
//           <button
//             type="button"
//             className="btn btn-danger border-rouded"
//             style={{ width: "11em",
//             marginLeft: "108em",
//             position: "fixed",
//             top: 30,
//             right: 0,}}
//             onClick={handleTurnOffAllDevices}
//             disabled={isTurningOff}
//           >
//             Turn Off All Devices
//           </button>
//         </div>
//       </div>
//     </div>
//   );
// }
