import { House, Room } from "@/entities";
import Link from "next/link";
import React from "react";

interface Props {
  rooms: Room[];
  houses: House[];
  onDeleteRoom: (id: number) => void;
}
export default function RoomsComponent({ rooms, houses, onDeleteRoom }: Props) {
  return (
    <div className="card-body">
      <div className="row m-5 d-flex items-center justify-center g-4">
        {rooms &&
          rooms.map((room) => (
            <div key={room.id} className="col-12 col-sm-6 col-md-4 col-lg-3">
              <div
                className="card text-center"
                style={{ boxShadow: "2px 4px 6px rgba(0.5, 0.5, 0.5, 0.2)" }}>
                <img
                  src={
                    "https://source.unsplash.com/random/1000x500?/" + room.name
                  }
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h4 className="card-title" style={{ height: "4.5rem" }}>
                    House:{" "}
                    {houses && houses.find((house) => house.id === room.house.id)?.name}
                  </h4>
                  <h5 className="card-title" style={{ height: "3rem" }}>
                  Room: {room.name}
                  </h5>
                  <h6 className="card-text">
                    Created at:{" "}
                    {new Date(room.createdAt).toLocaleDateString("en-EN")}
                  </h6>
                  <Link href={`/room/${room.id}`} className="btn btn-primary">
                    consult!!
                  </Link>
                  <button
                    className="btn btn-danger border-rouded m-2"
                    onClick={() => onDeleteRoom(Number(room.id))}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}
