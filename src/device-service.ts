import axios from "axios";
import { Device } from "./entities";

export const DeviceService = {
    async fetchAllDevices(){
        const response = await axios.get<Device[]>('http://localhost:8000/api/device');
        return response.data
    },
    async fetchOneDevice(id: number){
        const response = await axios.get<Device>('http://localhost:8000/api/device/' + id);
        return response.data
    },
    async fetchDeviceByRoom(id: number){
        const response = await axios.get<Device[]>('http://localhost:8000/api/device/room/' + id);
        return response.data
    },
    async createDevice(device:Device){
        const response = await axios.post<Device[]>('http://localhost:8000/api/device/' + device);
        return response.data
    },
    async updateDevice(device:Device){
        const response = await axios.put<Device>('http://localhost:8000/api/device/' + device.id, device);
        return response.data
    },
    async delteteDevice(id: number){
        const response = await axios.delete<Device>('http://localhost:8000/api/device/' + id);
        return response.data
    }
}
