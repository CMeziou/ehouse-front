import axios from "axios"
import { Device, Room } from "./entities"

export const RoomService = {
    async fetchRoomsByHouse(id: any) {
        const response = await axios.get<Room[]>('http://localhost:8000/api/room/house/' + id);
        return response.data; 
    },
    async fetchAllRooms() {
        const response = await axios.get<Room[]>('http://localhost:8000/api/room');
        return response.data; 
    },

    async fetchSingleRoom(id: any) {
        const response = await axios.get<Room>('http://localhost:8000/api/room/' + id);
        return response.data; 
    },

    async createRoom(room: Room) {
        const response = await axios.post<Room>('http://localhost:8000/api/room/', room);
        return response.data; 
    },
    async createRoomIntoHouse(room:Room) {
        const response = await axios.post<Room>('http://localhost:8000/api/house/' + room.house?.id, room);
        return response.data; 
    },
    async createDevice(device:Device) {
        const response = await axios.post<Room>('http://localhost:8000/api/room/' + device.room.id, device);
        return response.data; 
    },
    async updateRoom(room:Room){
        const response = await axios.put<Room>('http://localhost:8000/api/room/' + room.id,  room);
        return response.data
    },
    async delteteRoom(id: number){
        const response = await axios.delete<Room>('http://localhost:8000/api/room/' + id);
        return response.data
    }
}